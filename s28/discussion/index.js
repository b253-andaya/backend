// CRUD Operations
/*
	- CRUD operations are the heart of any backend application.
	- Mastering the CRUD operations is essential for any developer.
	- This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
	- Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// Insert documents (Create)
db.users.insertOne({
  firstName: "Grace",
  lastName: "Andaya",
  mobileNumber: "+639123456789",
  email: "grace@mail.com",
  company: "Zuitt",
});

db.users.insertMany([
  {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
      phone: "87654321",
      email: "stephenhawking@mail.com",
    },
    courses: ["Python", "React", "PHP", "CSS"],
    department: "none",
  },
  {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
      phone: "987654321",
      email: "neilarmstrong@mail.com",
    },
    courses: ["React", "Laravel", "Sass"],
    department: "none",
  },
]);

// Finding documents (Read/Retrieve)
/*
    Syntax:
        - db.collectionName.find() - find all
        - db.collectionName.find({field: value}); - all document that will match the criteria
        - db.collectionName.findOne({field: value}) - first document that will match the criteria
        - db.collectionName.findOne({}) - find first document
*/

db.users.find();
db.users.find({ firstName: "Stephen" });

//Mini Activity
/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true
	*/

db.courses.insertMany([
  {
    name: "Javascript 101",
    price: 5000,
    description: "Introduction to Javascript",
    isActive: true,
  },
  {
    name: "HTML 101",
    price: 2000,
    description: "Introduction to HTML",
    isActive: true,
  },
]);

db.users.findOne({});

// Updating/Replacing/Modifying documents (Update)
/*
    Syntax:
    Updating One Document
        db.collectionName.updateOne(
            {
                criteria: value
            },
            {
                $set: {
                    fieldToBeUpdated: value
                }
            }
        );

            - update the first matching document in our collection

    Multiple/Many Documents
    db.collectionName.updateMany(
        {
            criteria: value
        },
        {
            $set: {
                fieldToBeUpdated: value
            }
        }
    );

        - update multiple documents that matched the criteria
*/

db.users.insertOne({
  firstName: "Test",
  lastName: "Test",
  mobileNumber: "+639123456789",
  email: "test@mail.com",
  company: "none",
});

db.users.updateOne(
  {
    firstName: "Test",
  },
  {
    $set: {
      firstName: "Bill",
      lastName: "Gates",
      mobileNumber: "123456789",
      email: "billgates@mail.com",
      company: "Microsoft",
      status: "active",
    },
  }
);

/*
	Mini Activity:
		- Use updateOne() to change the isActive status of a course into false.
		-Use the updateMany() to set and add “enrollees” with a value of 10.
*/

db.course.updateOne(
  {
    name: "HTML 101",
  },
  {
    $set: {
      isActive: false,
    },
  }
);

db.courses.updateMany(
  {},
  {
    $set: {
      enrollees: 10,
    },
  }
);

// Rename fieldname
db.users.updateMany(
  {},
  {
    $rename: {
      department: "dept",
    },
  }
);

// Remove specific field
db.users.updateOne(
  {
    firstName: "Bill",
  },
  {
    $unset: {
      status: "active",
    },
  }
);

// Deleting Documents
/*
	Syntax:
		Deleting One Document:
			db.collectionName.deleteOne({"critera": "value"})

		Deleting Multiple Documents:
			db.collectionName.deleteMany({"criteria": "value"})
*/

db.users.deleteOne({
  company: "Microsoft",
});

db.users.deleteMany({
  dept: "none",
});

db.users.deleteMany({});
