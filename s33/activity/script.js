// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
console.log(fetch("https://jsonplaceholder.typicode.com/todos"));

// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => {
    const titles = data.map((item) => item.title);
    console.log(titles);
  })
  .catch((error) => console.error(error));

// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((json) => console.log(json));

// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((data) => {
    console.log(
      `Title: ${data.title}, Status: ${
        data.completed ? "Completed" : "Not Completed"
      }`
    );
  });

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/posts", {
  method: "POST",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "Study JavaScript",
    body: "Intense study",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PUT",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    id: 1,
    title: "Updated to do list",
    body: "Study Backend",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID
const updatedData = {
  title: "New Title",
  description: "New Description",
  status: true,
  date_completed: "2023-03-02",
  userId: 1,
};

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  body: JSON.stringify(updatedData),
  headers: {
    "Content-type": "application/json; charset=UTF-8",
  },
})
  .then((response) => response.json())
  .then((data) => console.log(data));

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/posts/2", {
  method: "PATCH",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "Programming",
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// Update a to do list item by changing the status to complete and add a date when the status was changed.
const dateCompleted = new Date().toISOString().substr(0, 10);

const updateData = {
  completed: true,
  completedAt: dateCompleted,
};

fetch("https://jsonplaceholder.typicode.com/todos/2", {
  method: "PATCH",
  body: JSON.stringify(updateData),
  headers: {
    "Content-type": "application/json; charset=UTF-8",
  },
})
  .then((response) => response.json())
  .then((data) => console.log(data));

// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/3", {
  method: "DELETE",
});
