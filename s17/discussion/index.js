// console.log("Hello!");

//[ SECTION ] Functions
// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
// Functions are mostly created to create complicated tasks to run several lines of code in succession
// They are also used to prevent repeating lines/blocks of codes that perform the same task/function
/*
	Syntax:
		function functionName() {
			code block (statement)
		}
*/
// function keyword - used to define javascript functions
// functionName - the function name. Functions are named to be able to use later in the code.
// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

function printName() {
  console.log("My name is Grace!");
}

//[ SECTION ] Function Invocation
//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
//It is common to use the term "call a function" instead of "invoke a function".

printName();

// Semicolons are used to separate executable JavaScript statements.

// [SECTION] FUNCTION DECLARATION VS EXPRESSIONS
//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
//It is common to use the term "call a function" instead of "invoke a function".
//Function Declarations

//A function can be created through function declaration by using the function keyword and adding a function name.

//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).

//declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declation

function declaredFunction() {
  console.log("Hello from declaredFunction()!");
}

declaredFunction();

// Function Expression
//A function can also be stored in a variable. This is called a function expression.

//A function expression is an anonymous function assigned to the variableFunction

//Anonymous function - a function without a name.

// variableFunction();
/* 
			error - function expressions, being stored 
			in a let or const variable, cannot be hoisted.
*/

let variableFunction = function () {
  console.log("Hello again!");
};

variableFunction();

let funcExpression = function funcName() {
  console.log("Hello from the other side!");
};

funcExpression();

// funcName(); error: funcName is not defined (variable of a function must be called/invoked)

declaredFunction = function () {
  console.log("Updated declaredFunction()");
};

declaredFunction(); //declared functions can be reassigned

funcExpression = function () {
  console.log("Updated funcExpression()");
};

funcExpression(); // either function declaration or expression can be reassigned

const constantFunc = function () {
  console.log("Initialized with const.");
};

constantFunc();

// constantFunc = function () {
//   console.log("Try to update.");
// };

// constantFunc();
//Error: Assignment to constant variable.

// [SECTION] FUNCTION SCOPING
/*	
	Scope is the accessibility (visibility) of variables.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function
*/

{
  let localVar = "Kudo Shinichi";
}

let globalVar = "Ms. Universe";

console.log(globalVar);
// console.log(localVar); Error: localVar not defined (since the variable is inside a local scope unless the console.log() is inside of the scope as well)

function showNames() {
  var functionVar = "Blossom";
  let functionLet = "Bubbles";
  const functionConst = "Buttercup";

  console.log(functionVar);
  console.log(functionLet);
  console.log(functionConst);
}

showNames();
/* 
    Error: variableName not defined because the variables are inside the function scope
*/
// console.log(functionVar);
// console.log(functionLet);
// console.log(functionConst);

// NESTED FUNCTIONS
// Nested Functions are functions that are inside a function. It will have acces to the variable name as they are within the same scope/code block.

function myNewFunc() {
  let name = "Jane";

  function nestedFunc() {
    let nestedName = "John";

    console.log(name);
  }

  //   console.log(nestedName); Error: not defined (global variables are only accessible inside the child. local variables cannot be accessed outside the scope)
  nestedFunc();
}

myNewFunc();
// nestedFunc(); Error: not defined since this is declared inside the myNewFunc
// console.log(name);

//[ SECTION ] Using alert()

//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

// alert("Hello Batch 253!");
// This will run immediately when the page loads.

// alert() can be used to show a message to the user from a later function invocation
// function showSampleAlert() {
//   alert("Hello, student!");
// }

// showSampleAlert();

console.log("Will only display when alert is dismissed.");
//Notes on the use of alert():
//Show only an alert() for short dialogs/messages to the user.
//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

//[SECTION] USING PROMPT ()
//prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

/* 
    Syntax:
        prompt("<dialogInString");
*/

// let samplePrompt = prompt("Enter your name: ");
// console.log("Hello, " + samplePrompt);
//prompt() returns an empty string when there is no input (OK). Or null if the user cancels the prompt() (Cancel button).

/* 
    Syntax:
        prompt("<dialogInString");
*/

/* 
    MINI-ACTIVITY
    1. Create a function that will ask a user for first name and last name.
    2. Concatenate the first and last name in a console log.
    3. console.log() a message "Welcome to my page!"
*/

// function printWelcomeMessage() {
//   let firstName = prompt("Enter your first name: ");
//   let lastName = prompt("Enter your last name: ");

//   console.log("Hello, " + firstName + " " + lastName + "!");
//   console.log("Welcome to my page!");
// }

// printWelcomeMessage();

// [SECTION] Function Naming Conventions
// Function names should be definitive of the task it will perform. It usually contains a verb.

function getFaveAnime() {
  let animes = ["Jigoku Shoujo", "Junji Ito", "Another"];
  console.log(animes);
}

// getFaveAnime();

//Avoid generic names to avoid confusion within your code.
function get() {
  // bad function name
  let pet = "cat";
  console.log(pet);
}

// get();

//Avoid pointless and inappropriate function names.
//Name your functions in small caps. Follow camelCase when naming variables and functions.

function a() {
  console.log("A");
}

a();
