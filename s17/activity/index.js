/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
//first function here:

function printUserInfo() {
  let myFullName = "John Doe";
  let myAge = 25;
  let myAddress = "123 street, Quezon City";
  let myCat = "Joe";
  let myDog = "Danny";

  console.log("printUserInfo()");
  console.log("Hello, I'm " + myFullName + ".");
  console.log("I am " + myAge + " years old.");
  console.log("I live in " + myAddress);
  console.log("I have a cat named " + myCat + ".");
  console.log("I have a dog named " + myDog + ".");
  console.log(undefined);
}

printUserInfo();
/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
//second function here:
function printFiveBands() {
  let band1 = "The Beatles";
  let band2 = "Taylor Swift";
  let band3 = "The Eagles";
  let band4 = "Rivermaya";
  let band5 = "Eraserheads";

  console.log("printFiveBands()");
  console.log(band1);
  console.log(band2);
  console.log(band3);
  console.log(band4);
  console.log(band5);
  console.log(undefined);
}

printFiveBands();
/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/

//third function here:
function printFiveMovies() {
  let myMovie1 = "Lion King";
  let myMovie2 = "Howl's Moving Castle";
  let myMovie3 = "Meet the Robinsons";
  let myMovie4 = "School of Rock";
  let myMovie5 = "Spirited Away";

  console.log("printFiveMovies()");
  console.log(myMovie1);
  console.log(myMovie2);
  console.log(myMovie3);
  console.log(myMovie4);
  console.log(myMovie5);
  console.log(undefined);
}

printFiveMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends() {
  let friend1 = "Eugene";
  let friend2 = "Dennis";
  let friend3 = "Vincent";

  console.log("printFriends()");
  console.log("These are my friends:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
  console.log(undefined);
}

printFriends();

// console.log(friend1);
// console.log(friend2);

//Do not modify
//For exporting to test.js
try {
  module.exports = {
    printUserInfo,
    printFiveBands,
    printFiveMovies,
    printFriends,
  };
} catch (err) {}
