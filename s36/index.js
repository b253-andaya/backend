// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Database connection
// Connecting to MongoDB Atlas
mongoose.connect(
  "mongodb+srv://admin:admin123@b253-andaya.ugqjfk2.mongodb.net/s36?retryWrites=true&w=majority",
  {
    // need new url not old
    useNewUrlParser: true,
    // re-routing server, if down find new one
    useUnifiedTopology: true,
  }
);
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas"));

app.use("/tasks", taskRoute);

if (require.main === module) {
  app.listen(port, () => console.log(`Server running at port ${port}.`));
}

module.exports = app;
