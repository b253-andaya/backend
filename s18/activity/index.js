// console.log("Hello World!");

function addNum(num1, num2) {
  console.log("Displayed sum of 5 and 15");
  console.log(num1 + num2);
}
addNum(5, 15);

function subNum(num1, num2) {
  console.log("Displayed difference of 20 and 5");
  console.log(num1 - num2);
}
subNum(20, 5);

function multiplyNum(num1, num2) {
  return num1 * num2;
}

function divideNum(num1, num2) {
  return num1 / num2;
}

const product = multiplyNum(50, 10);
const quotient = divideNum(50, 10);

console.log("The product of 50 and 10:");
console.log(product);

console.log("The quotient of 50 and 10:");
console.log(quotient);

function getCircleArea(radius) {
  const pi = Math.PI;
  const area = pi * radius * radius;
  return Number(area.toFixed(2)); // round off to 2 decimal places, thanks google

  // let area = 3.1416
  //return 3.1416*(radius**2)
}

let circleArea = getCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);

function getAverage(num1, num2, num3, num4) {
  return (num1 + num2 + num3 + num4) / 4;
}

let averageVar = getAverage(20, 40, 60, 80);
console.log("The result of getting the area of a circle with 15 radius:");
console.log(averageVar);

// CHECK IF SCORE IS PASSED
function checkIfPassed(yourScore, totalScore) {
  let percentage = (yourScore / totalScore) * 100;
  let isPassed = percentage > 75;
  return isPassed;
}

let isPassed = checkIfPassed(38, 50);
console.log("Is 38/50 a passing score?");
console.log(isPassed);

//Do not modify
//For exporting to test.js
try {
  module.exports = {
    addNum,
    subNum,
    multiplyNum,
    divideNum,
    getCircleArea,
    getAverage,
    checkIfPassed,
  };
} catch (err) {}
