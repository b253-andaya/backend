// console.log("Hello World");

//Objective 1
// Create a function called printNumbers() that will loop over a number provided as an argument.
//In the function, add a console to display the number provided.
//In the function, create a loop that will use the number provided by the user and count down to 0
//In the loop, create an if-else statement:

// If the value provided is less than or equal to 50, terminate the loop and show the following message in the console:
//"The current value is at " + count + ". Terminating the loop."

// If the value is divisible by 10, skip printing the number and show the following message in the console:
//"The number is divisible by 10. Skipping the number."

// If the value is divisible by 5, print the number.

let number = 100;

function printNumbers(number) {
  console.log("The number you provided is " + number);
  for (let count = number; count >= 0; count--) {
    if (count === 50) {
      console.log(
        "The current value is at " + count + ". Terminating the loop."
      );
      break;
    } else if (count % 10 === 0) {
      console.log("The number is divisible by 10. Skipping the number.");
    } else if (count % 5 === 0) {
      console.log(count);
    }
  }
}

printNumbers(number);

//Objective 2
// Create a for Loop that will iterate through the individual letters of the given string variable based on it's length
// Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
// Create an else statement that will add the current letter being looped to the given filteredString Variable
let string = "supercalifragilisticexpialidocious";
console.log(string);
let filteredString = "";

for (let i = 0; i < string.length; i++) {
  if (
    string[i] == "a" ||
    string[i] == "e" ||
    string[i] == "i" ||
    string[i] == "o" ||
    string[i] == "u"
  ) {
    continue;
  } else {
    filteredString += string[i];
  }
}
console.log(filteredString);

//Add code here

//Do not modify
//For exporting to test.js
try {
  module.exports = {
    printNumbers,
    filteredString,
  };
} catch (err) {}
