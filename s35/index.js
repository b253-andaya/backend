const express = require("express");
const mongoose = require("mongoose");
const { Schema } = mongoose;

const port = 4000;
const app = express();

app.use(express.json());

mongoose.connect(
  "mongodb+srv://admin:admin123@b253-andaya.ugqjfk2.mongodb.net/s35?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas"));

// ------ Mongoose schema -----
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema
const taskSchema = new Schema({
  name: String,
  status: {
    type: String,
    default: "pending",
  },
});

const Task = mongoose.model("Task", taskSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ----- Create todo list routes -----
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
app.post("/tasks", (req, res) => {
  Task.findOne({ name: req.body.name })
    .then((result, err) => {
      if (err) {
        return console.log(err);
      }
      console.log(result);
      if (result !== null && result.name === req.body.name) {
        return res.send("Duplicate task found");
      } else {
        let newTask = new Task({
          name: req.body.name,
        });

        //   newTask.save().then((savedTask, err) => {
        //     if (err) {
        //       return console.log(err);
        //     } else {
        //       return res.status(201).send("New task created");
        //     }
        //   });

        //.then and .catch chain:
        //.then() is used to handle the proper result/returned value of a function. If the function properly returns a value, we can run a separate function to handle it.
        //.catch() is used to handle/catch the error from the use of a function. So that if there is an error, we can properly handle it separate from the result.
        newTask
          .save()
          .then((result) => res.send(result))
          .catch((error) => res.send(error));
      }
    })
    .catch((error) => res.send(error));
});

// Getting all the tasks

// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) => {
  Task.find({})
    .then((result) => res.status(200).send(result))
    .catch((error) => res.send(error));
});

// ===================== ACTIVITY =========================
const usersSchema = new Schema({
  username: String,
  password: String,
});

const User = mongoose.model("User", usersSchema);

app.post("/signup", (req, res) => {
  if (req.body.username === "" || req.body.password === "") {
    return res.send("Invalid username or password.");
  }

  User.findOne({ username: req.body.username }).then((result) => {
    if (result != null) {
      return res.send(`Duplicate username found.`);
    }

    let newUser = new User({
      username: req.body.username,
      password: req.body.password,
    });

    newUser
      .save()
      .then(() => {
        console.log("New user registered");
        res.send("New user registered");
      })
      .catch((error) => {
        console.log(error);
        res.send("Error registering user");
      });
  });
});

app.listen(port, () => {
  console.log(`Listening on port ${port}.....`);
});
