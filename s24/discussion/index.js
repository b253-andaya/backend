// console.log("All is well");

/* ==================== ES6 ===================== */

// Exponent Operator
const firstNum = 8 ** 2; // ES6 update exponenent operator **
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals
/*
    - allows us to write strings without using the concatenation operator (+)
    - greatly helps with code readability
*/

let name = "John";

// pre-template literal
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals: " + message);

// Strings using template literals
// uses backticks (``)
// identifier natin is $ sign combined with curly braces {}
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals:  ${message}`);

// Multi-line using Template Literals
const anotherMessage = ` 
${name} attended a math competition. He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`;
console.log(anotherMessage);

/*
	- Template literals allow us to write strings with embedded JavaScript expressions
	- expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals
*/

const interestRate = 0.1;
const principal = 1000;

console.log(
  `The interest on your savings account: ${principal * interestRate}`
);

// Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability

    Syntax:
        let/const [variableName1, variableName2, variableName3] = arrayName;
*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(
  `Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`
);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(
  `Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`
);
console.log(`Hello ${fullName}! It's nice to meet you!`);

// Object Destructing
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects

    Syntax:    
        let/const {propertyName, propertyName, propertyName} = objectName;
*/
const person = {
  givenName: "Juana",
  maidenName: "Dela",
  familyName: "Cruz",
};

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(
  `Hello ${person.givenName} ${person.givenName} ${person.familyName}! It's good to see you again!`
);

// Object Destructuring
// Variable Destructuring
const { givenName, maidenName, familyName } = person;
const { givenName: fName, maidenName: mName, familyName: lName } = person; // to rename our propery names

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(
  `Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`
);

/*
    Passing the values using (function) destructured properties of the object 
    Syntax:
        function funcName ({propertyName,propertyName,propertyName}) {
            return statement (parameters)
        }

        funcName(object)
*/

// Function Destructuring
function getFullName({ givenName, maidenName, familyName }) {
  console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// Arrow Functions
/*
    - Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
    - Arrow functions also have implicit return which means we don't need to use the "return" keyword
*/

// Pre-Arrow Function
// function printFullName(firstName, lastName) {
//   console.log(`${firstName} ${lastName}`);
// }

// printFullName("John", "Smith");

// Arrow Function (=>)
/*
    Syntax:
        let/const variableName = (parameterA, parameterB) => {
            statement
        }

        or (if single-line no need for curly brackets)

        let/const variableName = () => expression;
*/
const printFullName = (firstName, lastName) => {
  console.log(`${firstName} ${lastName}`);
};

printFullName("Jane", "Smith");

const hello = () => {
  return "hello";
};

const sum = (x, y) => x + y;

let total = sum(1, 1);
console.log(total);

person.talk = () => "Hellooooooooooooo!";
console.log(person.talk());

// let personA = function Person(name1, age) {
//   this.name1 = name1;
//   this.age = age;
// };

// let maria = new Person("Maria", 22);
// console.log(maria);

// let { name1, age } = personA;
// console.log(name1 + age);

// Function with default Argument Value
const greet = (name = "User") => {
  return `Good morning, ${name}!`;
};

console.log(greet());
console.log(greet("Ariana"));

// Class-Based Object Blueprints
/*
    Allows the creation/instantation of objects using classes as blueprints 

    Syntax:
        class className {
            constructor (objectPropertyA, objectPropertyB){
                this.objectPropertyA = objectPropertyA;
                this.objectPropertyB = objectPropertyB;
            }
        }
*/

class Car {
  constructor(brand, model, year) {
    this.brand = brand;
    this.model = model;
    this.year = year;
  }
}

const myCar = new Car();
console.log(myCar);

// Values of properties may be assigned after creation/instantiation of an object
myCar.brand = "Ford";
myCar.model = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);

/*
    reduce()
    - Evaluates elements from left to right and returns/reduces the array into a single value
    - The "accumulator" parameter in the function stores the result for every iteration of the loop 
    - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop

    - How the "reduce" method works:
        1. The first/result element in the array is stored in the "accumulator" parameter
        2. The second/next element in the array is stored in the "currentValue" parameter
        3. An operation is performed on the two elements
        4. The loop repeats step 1-3 until all elements have been worked on

    Syntax:
        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
            return expression/operation
        })
*/

// Fibonacci
//                 3+3=6+4=10+5=15
let numbers = [1, 2, 3, 4, 5];

let iteration = 0;

let reducedArray = numbers.reduce(function (acc, curr) {
  // track the current iteration
  console.warn(`Current iteration: ${++iteration}`);

  console.log(`accumulator: ${acc}`);
  console.log(`currentValue: ${curr}`);

  // operation to return/reduce the array into a single value
  return acc + curr;
});

console.log(`Result of reduce() method: ${reducedArray}`);

let list = ["Hello", "Again", "Batch 253"];

let reducedJoin = list.reduce(function (acc, curr) {
  return `${acc} ${curr}`;
});

console.log(`Result of reduce() method: ${reducedJoin}`);
