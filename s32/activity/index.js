// Create a simple server and the following routes with their corresponding HTTP methods and responses:
// - If the url is http://localhost:4000/, send a response Welcome to Booking System
// - If the url is http://localhost:4000/profile, send a response Welcome to your profile!
// - If the url is http://localhost:4000/courses, send a response Here’s our courses available
// - If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
// - If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
// - If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources

const http = require("http");

const port = 4000;

const server = http.createServer((request, response) => {
  if (request.url == "/profile") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Welcome to your profile!");
  } else if (request.url == "/courses") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Here’s our courses available:");
  } else if (request.url == "/addCourse") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Add a course to our resources");
  } else if (request.url == "/updateCourse") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Update a course to our resources");
  } else if (request.url == "/archiveCourses") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Archive courses to our resources");
  } else {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Welcome to Booking System");
  }
});

server.listen(port);

console.log(`Server now accessible at localhost:${port}.`);
