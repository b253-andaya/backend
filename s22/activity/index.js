// console.log("Hello");

let registeredUsers = [
  "James Jeffries",
  "Gunther Smith",
  "Macie West",
  "Michelle Queen",
  "Shane Miguelito",
  "Fernando Dela Cruz",
  "Akiko Yukihime",
];

console.log(registeredUsers);

/* 
    1. Create a function called register which will allow us to register into the registeredUsers list.
        - This function should be able to receive a string.
        - Determine if the input username already exists in our registeredUsers array.
        - If it is, return the following message:
            - "Registration failed. Username already exists!"
        - If it is not, add the new username into the registeredUsers array and return the message:
            - "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.
*/

function register(registered) {
  let doesRegisterExist = registeredUsers.includes(registered);

  if (doesRegisterExist) {
    return "Registration failed. Username already exists!";
  } else {
    registeredUsers.push(registered);
    return "Thank you for registering!";
  }
}

/*
    2. Create a function called addFriend which will alow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
        - If it is, add the foundUser in our friendList array.
        - Then return the following message:
            - "You have added <registeredUser> as a friend!"
        - If it is not, return the following message:
            - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.
*/

let friendList = [];

function addFriend(username) {
  let isRegistered = registeredUsers.includes(username);

  if (isRegistered) {
    friendList.push(username);
    return "You have added " + username + " as a friend!";
  } else {
    return "User not found.";
  }
}

/* 
    3. Create a function called displayFriends which will alow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty, return the message:
            - "You curently have 0 friends. Add one first"
        - Invoke the function in the browser console.
*/

function displayFriends() {
  if (friendList.length === 0) {
    return "You currently have 0 friends. Add one first.";
  } else {
    friendList.forEach(function (friend) {
      console.log(friend);
    });
  }
}

/*
    4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty return the message:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty return the message:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function
*/

function displayNumberOfFriends() {
  if (friendList.length === 0) {
    return "You currently have 0 friends. Add one first.";
  } else {
    return "You currently have " + friendList.length + " friends.";
  }
}

/*
    5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty return a message:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

function deleteFriend() {
  if (friendList.length === 0) {
    return "You currently have 0 friends. Add one first.";
  } else {
    let removedFriend = friendList.pop();
    return "You have removed " + removedFriend + " from your friends list.";
  }
}

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

function deleteFriendByUsername(username) {
  const friendIndex = friendList.indexOf(username);

  if (friendIndex === -1) {
    return "User not found.";
  } else {
    friendList.splice(friendIndex, 1);
    return "You have removed " + username + " from your friends list.";
  }
}

//For exporting to test.js
try {
  module.exports = {
    registeredUsers,
    friendsList,
    register,
    addFriend,
    displayFriends,
    displayNumberOfFriends,
    deleteFriend,
  };
} catch (err) {}
