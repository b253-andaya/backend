const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
// router.post("/", (req, res) => {
//   courseController
//     .addCourse(req.body)
//     .then((resultFromController) => res.send(resultFromController))
//     .catch((err) => res.send(err));
// });

// ====================== ACTIVITY =====================
router.post("/", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .addCourse(req.body)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => {
          console.error(err);
          res.status(400).send({ error: `Error adding course` });
        });
    } else {
      res.send(false);
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: `Error decoding authorization header` });
  }
});

// ==================== LECTURE =====================
// Route for creating a course
router.post("/", auth.verify, (req, res) => {
  // const data = {
  // 	course: req.body,
  // 	isAdmin: auth.decode(req.headers.authorization).isAdmin
  // }

  // courseController.addCourse(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    courseController
      .addCourse(req.body)
      .then((resultFromController) => res.send(resultFromController))
      .catch((err) => res.send(err));
  } else {
    res.send(false);
  }
});

// Routes for retrieving all the courses
// router.get("/all", (req, res) => {
//   courseController
//     .getAllCourses()
//     .then((resultFromController) => res.send(resultFromController))
//     .catch((err) => {
//       console.error(err);
//       res.status(400).send({ error: `Error getting all courses` });
//     });
// });

router.get("/all", auth.verify, (req, res) => {
  // need middleware auth
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .getAllCourses()
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => {
          console.error(err);
          res.status(400).send({ error: `Error getting all courses` });
        });
    } else {
      res.send(false);
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: `Error decoding authorization header` });
  }
});

// Route for retrieving all the ACTIVE courses
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses
router.get("/", (req, res) => {
  courseController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => {
      console.error(err);
      res.status(500).send({ error: `Internal server error` });
    });
});

// Retrieving a specific course
/*
		Steps:
		1. Retrieve the course that matches the course ID provided from the URL
*/
router.get("/:courseId", (req, res) => {
  console.log(req.params.courseId);
  courseController
    .getCourse(req.params)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => {
      console.error(err);
      res.status(500).send({ error: `Internal server error` });
    });
});

// Route for updating a course
// JWT verification is needed for this route to ensure that a user is logged in before updating a course
// Allows us to export the "router" object that will be accessed in our "index.js" file
router.put("/:courseId", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .updateCourse(req.params, req.body)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => {
          console.error(err);
          res.status(400).send({ error: `Error updating the course` });
        });
    } else {
      res.send(false);
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: `Error decoding authorization header` });
  }
});

// ============================= ACTIVITY ==============================
// Route to archiving a course
// A "PUT"/"PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.patch("/:courseId/archive", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .archiveCourse(req.params, req.body)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => {
          console.error(err);
          res.status(400).send({ error: `Error updating the course` });
        });
    } else {
      res.send(false);
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: `Error decoding authorization header` });
  }
});

module.exports = router;
