// Connection to MongoDB database
const mongoose = require("mongoose");

// Schema/Data Model Structure
const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "User ID is required"],
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, "Product ID is required."],
      },
      quantity: {
        type: Number,
        default: 1,
      },
      price: {
        type: Number,
        required: [true, "Price is required."],
      },
    },
  ],
  totalAmount: {
    type: Number,
    required: [true, "Total amount of the order is required"],
  },
  status: {
    type: String,
    default: "pending",
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
});

// Exports the model from the file as a module
module.exports = mongoose.model("Order", orderSchema);
