// Middleware
const express = require("express");
const router = express.Router();

// Access Controllers
const userController = require("../controllers/userController");
const Order = require("../models/Order");
const auth = require("../auth");

// Route for user registration
router.post("/register", (req, res) => {
  userController
    .registerUser(req, res)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});

// Route for user login
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => {
      res.json(resultFromController);
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});

// Route for getting a user's details
router.get("/:userId/userDetails", (req, res) => {
  const authorizationHeader = req.headers.authorization;
  if (!authorizationHeader) {
    return res.status(401).json({ message: "Authorization header missing" });
  }

  const userData = auth.decode(authorizationHeader);

  const userId = req.params.userId;
  if (!userId) {
    return res
      .status(400)
      .json({ message: "User ID missing from request URL" });
  }

  // Check if user ID exists in the database
  const user = getUserById(userId);
  if (!user) {
    return res.status(400).json({ message: "Wrong user ID" });
  }

  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
});

// Route for getting All orders
router.get("/orders", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      userController
        .getAllOrders()
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => {
          console.error(err);
          res.status(400).send({ error: `Error getting all orders` });
        });
    } else {
      res.status(403).send({ message: `User cannot access this` });
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: `Error decoding authorization header` });
  }
});

// Route for getting the orders of a user
router.get("/myOrders", auth.verify, async (req, res) => {
  try {
    const decodedToken = auth.decode(req.headers.authorization);
    const userId = decodedToken.id;

    const orders = await userController.getOrdersForUser(userId);

    res.status(200).json({ orders });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Route for changing the user to an admin
router.put("/:id/make-admin", auth.verify, async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      const updatedUser = await userController.makeAdmin(req.params.id);
      res.send({ message: "User is now an admin", user: updatedUser });
    } else {
      // If the user is not an admin, make them an admin
      const updatedUser = await userController.makeAdmin(req.params.id);
      res.send({ message: "User is now an admin", user: updatedUser });
    }
  } catch (error) {
    console.error(error);
    res.status(400).send({ error: "Error updating user" });
  }
});

// Export the router as a middleware function
module.exports = router;
