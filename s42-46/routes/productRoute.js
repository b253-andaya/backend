// Middleware
const express = require("express");
const router = express.Router();

// Access Controllers
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product
router.post("/add-Product", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      productController
        .addProduct(req.body)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => res.send(err));
    } else {
      res.send(false);
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: `Error decoding authorization header` });
  }
});

// Route for retrieving all the active products
router.get("/", (req, res) => {
  productController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => {
      console.error(err);
      res.status(500).send({ error: `Internal server error` });
    });
});

// Route for retrieving all the products for admin
router.get("/all", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      productController
        .getAllProducts()
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => {
          console.error(err);
          res.status(400).send({ error: `Error getting all courses` });
        });
    } else {
      res.status(403).send("Users cannot access this site.");
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: `Error decoding authorization header` });
  }
});

// Route for retrieving a single product
router.get("/:productId", async (req, res) => {
  try {
    const product = await productController.getProduct(req.params.productId);
    res.send(product);
  } catch (err) {
    console.error(err);
    if (err instanceof ProductNotFoundError) {
      res.status(404).send({ error: `Product not found` });
    } else {
      res.status(500).send({ error: `Internal server error` });
    }
  }
});

// Route for updating a product (Admin Only)
router.put("/:productId", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      productController
        .updateProduct(req.params, req.body)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => {
          console.error(err);
          res.status(400).send({ error: `Error updating the product` });
        });
    } else {
      res.send(false);
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: `Error decoding authorization header` });
  }
});

// Route for archiving a product (Admin Only)
router.put("/:productId/archive", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      productController
        .archiveProduct(req.params, req.body)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => {
          console.error(err);
          res.status(400).send({ error: `Error updating the course` });
        });
    } else {
      res.send(false);
    }
  } catch (err) {
    console.error(err);
    res.status(400).send({ error: `Error decoding authorization header` });
  }
});

// Export the router as a middleware function
module.exports = router;
