// Middlewares
const Product = require("../models/Product");
const User = require("../models/User");

// Controller for creating a product
module.exports.addProduct = async (reqBody) => {
  // Validate request body
  if (!reqBody.name) {
    return "Please input a valid name";
  }
  if (!reqBody.description) {
    return "Please input a valid description";
  }
  if (!reqBody.price) {
    return "Please input a valid price";
  }

  // Check if product with same name already exists
  const existingProduct = await Product.findOne({ name: reqBody.name });
  if (existingProduct) {
    return "Product has already been added";
  }

  // Creates a variable "newProduct" and instantiates a new "Product" object using the mongoose model
  // Uses the information from the request body to provide all the necessary information
  const newProduct = new Product({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  });

  return newProduct
    .save()
    .then((product) => {
      return "Product added successfully";
    })
    .catch((err) => {
      console.error(err);
      throw new Error(`Error adding product.`);
    });
};

// Controller for getting all active products
module.exports.getAllActive = () => {
  return Product.find({ isActive: true })
    .then((result) => result)
    .catch((err) => err);
};

// Controller for getting all products for admin
module.exports.getAllProducts = () => {
  return Product.find({})
    .then((result) => result)
    .catch((err) => {
      console.error(err);
      throw new Error(`Error finding products.`);
    });
};

// Controller for retrieving a single product
module.exports.getProduct = (productId) => {
  return Product.findById(productId)
    .then((result) => {
      if (!result) {
        throw new ProductNotFoundError(
          `Product with ID ${productId} not found`
        );
      }
      return result;
    })
    .catch((err) => {
      console.error(err);
      throw new Error(`Error finding product.`);
    });
};

// Controller for updating a product (Admin Only)
module.exports.updateProduct = (reqParams, reqBody) => {
  if (!reqBody.name) {
    throw new Error("Please input an updated name.");
  }
  if (!reqBody.description) {
    throw new Error("Please input an updated description.");
  }
  if (!reqBody.price) {
    throw new Error("Please input an updated price.");
  }

  let updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };

  return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
    .then((product) => {
      if (!product) {
        throw new Error(`Product with id ${reqParams.productId} not found.`);
      }
      return "Successfully updated the product.";
    })
    .catch((err) => {
      console.error(err);
      throw new Error(`Error updating product.`);
    });
};

// Controller for Archiving a Product (Admin Only)
module.exports.archiveProduct = (reqParams, reqBody) => {
  let updatedProduct = {
    isActive: false,
  };

  return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
    .then((product) => true)
    .catch((err) => {
      console.error(err);
      throw new Error(`Error updating course.`);
    });
};
