// console.log("Konichiwa");

// [SECTION] ARRAY
// Array in programming is simply alist of data.

let studentNumberA = "2023-1921";
let studentNumberB = "2023-1922";
let studentNumberC = "2023-1923";
let studentNumberD = "2023-1924";

let studentNumber = ["2023-1921", "2023-1922", "2023-1923", "2023-1924"];

/* 
    - Arrays are used to store multiple related values in a single variable
	- They are declared using square brackets ([]) also known as "Array Literals"
	- Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
	- Arrays also provide access to a number of functions/methods that help in achieving this
	- A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
    - Majority of methods are used to manipulate information stored within the same object
	- Arrays are also objects which is another data type
	- The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"

    Syntax:
        let/const arrayName = [elementA, elementB, elementC...];
*/

// COMMON EXAPLES OF ARRAYS
let grades = [98.5, 94.3, 89.2, 90.1]; // number array
let computerBrands = ["Acer", "Asus", "Lenovo", "MSI"]; // string array

// Possible use of an array but THIS IS NOT RECOMMENDED
let mixedArray = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArray);

// Alternative way to write arrays
let myTasks = ["drink html", "eat javascript", "inhale css", "bake sass"];

let city1 = "Tokyo",
  city2 = "Manila",
  city3 = "Berlin";

// Creating an array with values from variables
let cities = [city1, city2, city3];

console.log(myTasks);
console.log(cities);

// ======================================
// [SECTION] LENGTH PROPERTY
// The .length propoerty allows us to get and set the total number of items/elements in an array

console.log(myTasks.length);
console.log(cities.length);

// .length property in string allows us to get the total number of characters including the white spaces
let fullName = "Sana Minatozaki";
console.log(fullName.length);

let blankArray = [];
console.log(blankArray.length);
console.log(blankArray);

// Delete Item from Array
// deleting last item of an array
myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

// using decrementation
cities.length--;
console.log(cities);

// We cannot delete using .length property in strings
fullName.length = fullName.length - 1;
console.log(fullName.length);

fullName.length--;
console.log(fullName);

// lengthen array
let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles.length);
theBeatles.length++;
console.log(theBeatles);
console.log(theBeatles.length - 1);

// Assigning a value to a specific array index
// arrayName[index] = "new value"
theBeatles[4] = "Sir Jimbo";
console.log(theBeatles);
/*
	Accessing Elements of an Array

	Syntax:
		arrayName[index];

*/
console.log(theBeatles[2]);
console.log(theBeatles[0]);

// console.log(grades[20]);
// grades[20] = 90.5;
// console.log(grades[20]);
// console.log(grades);
// Although this is possible, but this is NOT a good practice

let koponanNiEugene = ["Eugene", "Vincent", "Alfred", "Dennis"];
console.log(koponanNiEugene[3]);
console.log(koponanNiEugene[1]);

let member = koponanNiEugene[2];
console.log(member);

// Reassigning value/s in array
console.log("Array before reassignment");
console.log(koponanNiEugene);
koponanNiEugene[2] = "Jeremiah";
console.log("Array after reassignment:");
console.log(koponanNiEugene);

// Accessing the last element of an array
let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegend.length - 1;
console.log(bullsLegend[lastElementIndex]);
console.log(bullsLegend[bullsLegend.length - 1]);
console.log(bullsLegend[bullsLegend.length - 2]);

// Adding items into the array
let newArr = [];
console.log(newArr);
console.log(newArr[0]);
newArr[0] = "Tzuyu";
console.log(newArr);
newArr[1] = "Mina";
console.log(newArr);

console.log(newArr.length);
newArr[newArr.length] = "Momo";
console.log(newArr);
console.log(newArr.length);
newArr[newArr.length] = "Sana";
console.log(newArr);

let superHeroes = ["Iron-Man", "Spider-Man", "Captain America"];

function marvelHeroes(heroName) {
  superHeroes[superHeroes.length] = heroName;
}

marvelHeroes("Ant-Man");
console.log(superHeroes);
marvelHeroes("Scarlet Witch");
console.log(superHeroes);

// Part 2

function getHeroByIndex(index) {
  return superHeroes[index];
}

let heroFound = getHeroByIndex(2);
console.log(heroFound);

// Loops over an Array

for (let index = 0; index < newArr.length; index++) {
  console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 50, 98];
for (let index = 0; index < numArr.length; index++) {
  if (numArr[index] % 5 === 0) {
    console.log(numArr[index] + " is divisibile by 5.");
  } else {
    console.log(numArr[index] + " is not divisible by 5.");
  }
}

// Multidimensional Array

let chessBoard = [
  ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"], // index 0
  ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"], // index 0
  ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"], // index 0
  ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"], // index 0
  ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"], // index 0
  ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"], // index 0
  ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"], // index 0
  ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"], // index 0
];

console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[7][4]);
